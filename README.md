# Trabalho Web


## Adicionar/atualizar versões direto do Git

- [ ] [Criar](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) ou [enviar](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) arquivos
- [ ] [Adicionar arquivos com linhas de comando](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) ou dar push num repositório existente com os comandos:

```
cd existing_repo
git remote add origin https://gitlab.com/FellipeGG/trabalho-web.git
git branch -M main
git push -uf origin main
```

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Descrição
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.
