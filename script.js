document.addEventListener('DOMContentLoaded', () => {
  // Variáveis globais do jogo
  let palavra = '';
  let letrasAdivinhadas = [];
  let tentativasRestantes = 10;

  const listaPalavras = ['rafamaconheira', 'sus', 'uberjames', 'javamedamedo', 'jhonxina']; // Lista de palavras

  // Função para escolher uma palavra aleatória
  function escolherPalavraAleatoria() {
      const indiceAleatorio = Math.floor(Math.random() * listaPalavras.length);
      palavra = listaPalavras[indiceAleatorio];
  }

  // Função para inicializar as variáveis necessárias
  function iniciarJogo() {
      escolherPalavraAleatoria();
      letrasAdivinhadas = [];
      tentativasRestantes = 10;
      atualizarTelaJogo();
  }

  // Função para exibir a palavra como "_" para cada letra não adivinhada
  function atualizarTelaJogo() {
      const containerPalavra = document.getElementById('container-palavra');
      containerPalavra.innerHTML = '';

      for (let i = 0; i < palavra.length; i++) {
          const letra = palavra[i];

          if (letrasAdivinhadas.includes(letra)) {
              containerPalavra.innerHTML += `<span>${letra}</span>`;
          } else {
              containerPalavra.innerHTML += `<span>_</span>`;
          }
      }

      const containerTentativas = document.getElementById('container-tentativas');
      containerTentativas.textContent = `Tentativas Restantes: ${tentativasRestantes}`;
  }

  // Função para capturar a entrada do teclado do usuário e atualizar o jogo de acordo
  function lidarComTeclaPressionada(event) {
      const teclaPressionada = event.key.toLowerCase();

      if (teclaPressionada.match(/[a-z]/i) && !letrasAdivinhadas.includes(teclaPressionada)) {
          letrasAdivinhadas.push(teclaPressionada);

          if (!palavra.includes(teclaPressionada)) {
              tentativasRestantes--;
          }

          atualizarTelaJogo();
          verificarResultadoJogo();
      }
  }

  // Função para verificar se o jogo foi vencido ou perdido
  function verificarResultadoJogo() {
      const containerPalavra = document.getElementById('container-palavra');
      const palavraExibida = containerPalavra.textContent;

      if (palavraExibida === palavra) {
          alert('Parabéns! Você venceu o jogo! A palavra correta é ' + palavra);
          iniciarJogo();
      } else if (tentativasRestantes === 0) {
          alert('Fim de jogo! Você perdeu. A palavra correta é ' + palavra);
          iniciarJogo();
      }
  }

  // Chame a função iniciarJogo para iniciar o jogo
  iniciarJogo();

  // Adicione um event listener para capturar as teclas pressionadas pelo usuário
  document.addEventListener('keydown', lidarComTeclaPressionada);
});